<?php

use App\Persona;

class MYPDF extends TCPDF {

    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

$pdf = new MYPDF();
$pdf->setPrintFooter(true);
$pdf->SetAutoPageBreak(TRUE, 10);
$pdf->AddPage();

$pdf->Cell(0, 10, 'Reporte de personas', 0, 10, 'C', false);
$pdf->Ln(10);

$pdf->SetFillColor(100, 100, 100);

$pdf->Cell(40, 10, 'Apellido paterno', 1, 0, 'C', true);
$pdf->Cell(40, 10, 'Apellido materno', 1, 0, 'C', true);
$pdf->Cell(40, 10, 'Nombres', 1, 0, 'C', true);
$pdf->Cell(0, 10, 'Email', 1, 0, 'C', true);
$pdf->Ln(10);

$texto = isset($texto) ? $texto : '';
$resultado = Persona::buscarPorTexto('')->get();

foreach ($resultado as $key => $value) {
    $pdf->Cell(40, 8, $value->apellido_paterno, 1, 0, 'L', false);
    $pdf->Cell(40, 8, $value->apellido_materno, 1, 0, 'L', false);
    $pdf->Cell(40, 8, $value->nombres, 1, 0, 'L', false);
    $pdf->Cell(0, 8, $value->email, 1, 0, 'L', false);
    $pdf->Ln(8);
}

$path = storage_path('app/personas.pdf');
$pdf->Output($path, 'F');
