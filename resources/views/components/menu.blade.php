@foreach ($listaMenu as $menu)
    @if (Route::has($menu['ruta']))
        @php
            $clase = '';
            $icono = '';
        @endphp
        @if($opcionActiva === $menu['ruta'])
            @php
                $clase = 'active';
                $icono = '<i class="fas fa-angle-right"></i> '
            @endphp
        @endif
        <li class="nav-item {{ $clase }}">
            <a class="nav-link" href="{{ route($menu['ruta']) }}">
                {!! $icono !!}{{ $menu['nombre'] }}
            </a>
        </li>
    @endif
@endforeach
