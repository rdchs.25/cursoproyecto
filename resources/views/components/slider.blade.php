<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
        @php
        $active = 'active';
        $contador = 0;
        @endphp
        @foreach ($sliders as $slider)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $contador }}" class="{{ $active }}"></li>
            @php
            $active = '';
            $contador++;
            @endphp
        @endforeach
    </ol>
    <div class="carousel-inner" role="listbox">
        @php
        $active = 'active';
        $contador = 0;
        @endphp
        @foreach ($sliders as $slider)
            <div class="carousel-item {{ $active }}">
                <img class="d-block img-fluid" src="{{ $slider->imagen_url }}" alt="First slide">
            </div>
            @php
            $active = '';
            $contador++;
            @endphp
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
