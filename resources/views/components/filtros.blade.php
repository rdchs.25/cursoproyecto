<div class="col-lg-3">
    <h1 class="my-4">Tipos de cursos</h1>
    <div class="list-group">
        @foreach ($tipos as $tipo)
            <a href="{{ route('filtro.tipocurso.index', $tipo->slug) }}" class="list-group-item">{{ $tipo->nombre }}</a>
        @endforeach
    </div>
</div>
