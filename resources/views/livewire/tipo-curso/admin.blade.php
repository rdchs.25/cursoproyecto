<div class="card-body">
    {{-- Mensajes --}}
    @if (session('mensaje'))
        <div class="alert alert-success" role="alert">
            {{ session('mensaje') }}
        </div>
    @endif
    {{-- Formulario de búsqueda --}}
    <div class="form-inline">
        {{-- Boton crear --}}
        <button type="button" data-toggle="modal" data-target="#modalNuevoTipoCurso" class="btn btn-success ml-2 mb-2">
            <i class="fas fa-plus-circle"></i> Nuevo
        </button>
        {{-- Botón reporte PDF --}}
        <a target="__blank" href="{{ route('tipocurso.reportelistadopdf') }}" class="btn btn-secondary ml-2 mb-2">
            <i class="fas fa-file-excel"></i> Reporte
        </a>
        {{-- Botón reporte excel --}}
        <a target="__blank" href="{{ route('tipocurso.reportelistadoexcel') }}" class="btn btn-secondary ml-2 mb-2">
            <i class="fas fa-file-pdf"></i> Reporte
        </a>
    </div>
    {{-- MODAL DE NUEVO --}}
    @include('livewire.tipo-curso.crear')
    {{-- MODAL PARA EDITAR --}}
    @include('livewire.tipo-curso.editar')
    {{-- MODAL PARA ELIMINAR --}}
    @include('livewire.tipo-curso.eliminar')
    <div class="table-responsive-xl">
        <table class="table table-sm table-bordered table-hover">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th>Nombre</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listado as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td class="text-center">
                            <button wire:click="editar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEditarTipoCurso" class="btn btn-warning ml-2 mb-2">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <button wire:click="eliminar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEliminarTipoCurso" class="btn btn-danger ml-2 mb-2">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="thead-dark">
                <tr class="text-center">
                    <th>Nombre</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="table-responsive-xl">
        {{ $listado->links() }}
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        // evento de javascript
        window.livewire.on('cerrarModalNuevoTipoCurso', () => {
            $('#modalNuevoTipoCurso').modal('hide');
        });
        window.livewire.on('cerrarModalEditarTipoCurso', () => {
            $('#modalEditarTipoCurso').modal('hide');
        });
        window.livewire.on('cerrarModalEliminarTipoCurso', () => {
            $('#modalEliminarTipoCurso').modal('hide');
        });
    </script>
@endpush
