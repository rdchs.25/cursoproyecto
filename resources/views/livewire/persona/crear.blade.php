<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalNuevoPersona" tabindex="-1" role="dialog"
    aria-labelledby="modalNuevoPersonaLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalNuevoPersonaLabel">Registrar persona</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="apellido_paterno">
                            Apellido paterno
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="apellido_paterno" placeholder="Apellido paterno"
                                wire:model.defer="apellido_paterno">
                            @error('apellido_paterno')
                            <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <label class="col-md-2 col-form-label text-md-right" for="apellido_materno">
                            Apellido materno
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="apellido_materno" placeholder="Apellido materno"
                                wire:model.defer="apellido_materno">
                            @error('apellido_materno') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="nombres">Nombres</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="nombres" placeholder="Nombres"
                                wire:model.defer="nombres">
                            @error('nombres') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <label class="col-md-2 col-form-label text-md-right" for="email">Correo eletrónico</label>
                        <div class="col-md-4">
                            <input type="email" class="form-control" id="email" placeholder="Correo electrónico"
                                wire:model.defer="email">
                            @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="celular">Celular</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="celular" placeholder="Celular"
                                wire:model.defer="celular">
                            @error('celular') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="departamento_id">Departamento</label>
                        <div class="col-md-4">
                            <select wire:model='departamento_id' class="custom-select" name="departamento_id"
                                id="departamento_id">
                                <option value='0' selected>Seleccionar</option>
                                @foreach ($departamentos as $departamento)
                                    <option value='{{ $departamento['id'] }}'>{{ $departamento['nombre'] }}</option>
                                @endforeach
                            </select>
                            @error('departamento_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <label class="col-md-2 col-form-label text-md-right" for="provincia_id">Provincia</label>
                        <div class="col-md-4">
                            <select wire:model='provincia_id' class="custom-select" name="provincia_id"
                                id="provincia_id">
                                <option value="0">Seleccionar</option>
                                @foreach ($provincias as $provincia)
                                    <option value="{{ $provincia['id'] }}">{{ $provincia['nombre'] }}</option>
                                @endforeach
                            </select>
                            @error('provincia_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="distrito_id">Distrito</label>
                        <div class="col-md-4">
                            <select wire:model.defer='distrito_id' class="custom-select" name="distrito_id"
                                id="distrito_id">
                                <option value="0">Seleccionar</option>
                                @foreach ($distritos as $distrito)
                                    <option value="{{ $distrito['id'] }}">{{ $distrito['nombre'] }}</option>
                                @endforeach
                            </select>
                            @error('distrito_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <label class="col-md-2 col-form-label text-md-right" for="direccion">Dirección</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="direccion" placeholder="Dirección"
                                wire:model.defer="direccion">
                            @error('direccion') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="nuevo"
                    class="btn btn-primary close-modal">
                    <i class="fas fa-check-circle"></i> Registrar
                </button>
            </div>
        </div>
    </div>
</div>
