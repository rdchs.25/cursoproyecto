<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalNuevoPersona" tabindex="-1" role="dialog"
    aria-labelledby="modalNuevoPersonaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalNuevoPersonaLabel">Registrar persona</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="apellido_paterno">DNI</label>
                        <div class="input-group mb-3">
                            <input wire:model.defer='dni' type="text" class="form-control" placeholder="Escribe DNI"
                                aria-label="Escribe DNI" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button type="button" wire:click.prevent='buscarDni' class="input-group-text" id="basic-addon2">Buscar</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellido_paterno">Apellido paterno</label>
                        <input type="text" class="form-control" id="apellido_paterno"
                            placeholder="Ingresa apellido paterno" wire:model.defer="apellido_paterno">
                        @error('apellido_paterno')
                            <span class="text-danger error">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="apellido_materno">Apellido materno</label>
                        <input type="text" class="form-control" id="apellido_materno"
                            placeholder="Ingresa apellido materno" wire:model.defer="apellido_materno">
                        @error('apellido_materno') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="nombres">Nombres</label>
                        <input type="text" class="form-control" id="nombres" placeholder="Ingresa nombres"
                            wire:model.defer="nombres">
                        @error('nombres') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Correo eletrónico</label>
                        <input type="email" class="form-control" id="email" placeholder="Ingresa correo electrónico"
                            wire:model.defer="email">
                        @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control" id="celular" placeholder="Ingresa celular"
                            wire:model.defer="celular">
                        @error('celular') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input type="text" class="form-control" id="direccion" placeholder="Ingresa dirección"
                            wire:model.defer="direccion">
                        @error('direccion') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="departamento_id">Departamento</label>
                        <select wire:model='departamento_id' class="custom-select" name="departamento_id"
                            id="departamento_id">
                            <option value='0' selected>Seleccionar</option>
                            @foreach ($departamentos as $departamento)
                                <option value='{{ $departamento['id'] }}'>{{ $departamento['nombre'] }}</option>
                            @endforeach
                        </select>
                        @error('departamento_id') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="provincia_id">Provincia</label>
                        <select wire:model='provincia_id' class="custom-select" name="provincia_id" id="provincia_id">
                            <option value="0">Seleccionar</option>
                            @foreach ($provincias as $provincia)
                                <option value="{{ $provincia['id'] }}">{{ $provincia['nombre'] }}</option>
                            @endforeach
                        </select>
                        @error('provincia_id') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="distrito_id">Distrito</label>
                        <select wire:model.defer='distrito_id' class="custom-select" name="distrito_id"
                            id="distrito_id">
                            <option value="0">Seleccionar</option>
                            @foreach ($distritos as $distrito)
                                <option value="{{ $distrito['id'] }}">{{ $distrito['nombre'] }}</option>
                            @endforeach
                        </select>
                        @error('distrito_id') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="nuevo"
                    class="btn btn-primary close-modal">
                    <i class="fas fa-check-circle"></i> Registrar
                </button>
            </div>
        </div>
    </div>
</div>
