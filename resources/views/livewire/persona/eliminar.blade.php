<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalEliminarPersona" tabindex="-1" role="dialog"
    aria-labelledby="modalEliminarPersonaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalEliminarPersonaLabel">Eliminar persona</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                    <strong>¿Estás seguro de eliminar el registro:
                        {{ $apellido_paterno . ' ' . $apellido_materno }}?
                    </strong>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="destroy"
                    class="btn btn-danger close-modal">
                    <i class="fas fa-check-circle"></i> Eliminar
                </button>
            </div>
        </div>
    </div>
</div>
