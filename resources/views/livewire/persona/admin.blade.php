<div class="card-body">
    {{-- Mensaje de confirmación --}}
    @if(session('mensaje'))
        <div class="alert alert-primary" role="alert">
            <strong>{{ session('mensaje') }}</strong>
        </div>
    @endif
    {{-- Formulario de búsqueda --}}
    <form class="form-inline" wire:submit.prevent='buscar'>
        <label class="form-label mb-2 mr-sm-2">Apellidos o nombres</label>
        <input type="text" class="form-control mb-2 mr-sm-2" wire:model.defer="textoBusqueda" />
        <button type="submit" class="btn btn-primary ml-2 mb-2">
            <i class="fas fa-search"></i> Buscar
        </button>
        <button type="button" data-toggle="modal" data-target="#modalNuevoPersona" class="btn btn-success ml-2 mb-2">
            <i class="fas fa-user"></i> Nuevo
        </button>
        {{-- Botón reporte EXCEL --}}
        <button class="btn btn-secondary ml-2 mb-2" wire:click='reporteExcel' >
            <i class="fas fa-file-excel"></i> Reporte
        </button>
        {{-- Botón reporte PDF --}}
        <button class="btn btn-secondary ml-2 mb-2" wire:click='reportePdf' >
            <i class="fas fa-file-pdf"></i> Reporte
        </button>
    </form>
    {{-- MODAL DE NUEVO --}}
    @include('livewire.persona.nuevo')
    {{-- MODAL DE EDITAR --}}
    @include('livewire.persona.editar')
    {{-- MODAL DE ELIMINAR --}}
    @include('livewire.persona.eliminar')
    {{-- MODAL DE EDITAR --}}
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>Nombre completo</th>
                <th>Email</th>
                <th colspan="2">Operaciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($personas as $persona)
                <tr>
                    <td>{{ $persona->nombre_completo }}</td>
                    <td>{{ $persona->email }}</td>
                    <td>
                        <button data-toggle="modal" wire:click="editar({{ $persona->id }})" data-target="#modalEditarPersona" class="btn btn-warning">
                            <i class="fas fa-pencil-alt"></i> Editar
                        </button>
                    </td>
                    <td>
                        <button data-toggle="modal" wire:click="eliminar({{ $persona->id }})" data-target="#modalEliminarPersona" class="btn btn-danger">
                            <i class="fas fa-trash"></i> Eliminar
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $personas->links() }}
    @push('scriptsjs')
        <script type="text/javascript">
            // evento de javascript
            window.livewire.on('cerrarModalNuevoPersona', () => {
                $('#modalNuevoPersona').modal('hide');
            });
            // evento de javascript
            window.livewire.on('cerrarModalEditarPersona', () => {
                $('#modalEditarPersona').modal('hide');
            });
            // evento de javascript
            window.livewire.on('cerrarModalEliminarPersona', () => {
                $('#modalEliminarPersona').modal('hide');
            });
        </script>
    @endpush
</div>
@push('scripts')
    <script type="text/javascript">
        // evento de javascript
        window.livewire.on('cerrarModalNuevoPersona', () => {
            $('#modalNuevoPersona').modal('hide');
        });
        window.livewire.on('cerrarModalEditarPersona', () => {
            $('#modalEditarPersona').modal('hide');
        });
        window.livewire.on('cerrarModalEliminarPersona', () => {
            $('#modalEliminarPersona').modal('hide');
        });

    </script>
@endpush
