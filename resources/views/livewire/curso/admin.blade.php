<div class="card-body">
    {{-- Mensajes --}}
    @if (session('mensaje'))
        <div class="alert alert-success" role="alert">
            {{ session('mensaje') }}
        </div>
    @endif
    {{-- Formulario de búsqueda --}}
    <form class="form-inline" wire:submit.prevent='buscar'>
        <label class="form-label mb-2 mr-sm-2">Nombre</label>
        <input type="text" class="form-control mb-2 mr-sm-2" wire:model.defer="textoBusqueda" />
        <button type="submit" class="btn btn-primary ml-2 mb-2">
            <i class="fas fa-search"></i> Buscar
        </button>
        {{-- Boton crear --}}
        <button type="button" wire:click='crear' data-toggle="modal" data-target="#modalNuevoCurso" class="btn btn-success ml-2 mb-2">
            <i class="fas fa-plus-circle"></i> Nuevo
        </button>
        {{-- Botón reporte PDF --}}
        <a target="__blank" href="{{ route('curso.reportelistadopdf') }}" class="btn btn-secondary ml-2 mb-2">
            <i class="fas fa-file-excel"></i> Reporte
        </a>
        {{-- Botón reporte excel --}}
        <a target="__blank" href="{{ route('curso.reportelistadoexcel') }}" class="btn btn-secondary ml-2 mb-2">
            <i class="fas fa-file-pdf"></i> Reporte
        </a>
    </form>
    {{-- MODAL DE NUEVO --}}
    @include('livewire.curso.crear')
    {{-- MODAL PARA EDITAR --}}
    @include('livewire.curso.editar')
    {{-- MODAL PARA ELIMINAR --}}
    @include('livewire.curso.eliminar')
    <div class="table-responsive-xl">
        <table class="table table-sm table-bordered table-hover">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th>Tipo de curso</th>
                    <th>Nombre</th>
                    <th>Nombre corto</th>
                    <th>Imagen</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listado as $item)
                    <tr>
                        <td class="align-middle">{{ $item->tipoCurso->nombre }}</td>
                        <td class="align-middle">{{ $item->nombre }}</td>
                        <td class="align-middle">{{ $item->nombre_corto }}</td>
                        <td class="text-center">
                            <img src="{{ $item->imagen_url }}" height="100px" alt="Imagen Curso">
                        </td>
                        <td class="text-center">
                            <button wire:click="editar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEditarCurso" class="btn btn-warning ml-2 mb-2">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <button wire:click="eliminar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEliminarCurso" class="btn btn-danger ml-2 mb-2">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="thead-dark">
                <tr class="text-center">
                    <th>Tipo de curso</th>
                    <th>Nombre</th>
                    <th>Nombre corto</th>
                    <th>Imagen</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="table-responsive-xl">
        {{ $listado->links() }}
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        // evento de javascript
        window.livewire.on('cerrarModalNuevoCurso', () => {
            $('#modalNuevoCurso').modal('hide');
        });
        window.livewire.on('cerrarModalEditarCurso', () => {
            $('#modalEditarCurso').modal('hide');
        });
        window.livewire.on('cerrarModalEliminarCurso', () => {
            $('#modalEliminarCurso').modal('hide');
        });

    </script>
@endpush
