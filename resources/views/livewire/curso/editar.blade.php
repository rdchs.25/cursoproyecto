<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalEditarCurso" tabindex="-1" role="dialog"
    aria-labelledby="modalEditarCursoLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalEditarCursoLabel">Actualizar curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="tipo_curso_id">Tipo de curso</label>
                        <div class="col-md-8">
                            <select wire:model.defer='tipo_curso_id' class="custom-select" name="tipo_curso_id"
                                id="tipo_curso_id">
                                <option value='0' selected>Seleccionar</option>
                                @foreach ($tipo_cursos as $item)
                                    <option value='{{ $item['id'] }}'>{{ $item['nombre'] }}</option>
                                @endforeach
                            </select>
                            @error('tipo_curso_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="nombre">Nombre</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="Nombre" wire:model.defer="nombre">
                            @error('nombre') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="nombre_corto">Nombre corto</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="Nombre corto"
                                wire:model.defer="nombre_corto">
                            @error('nombre_corto') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="nombre_corto">Imagen</label>
                        <div class="col-md-8">
                            <div class="custom-file">
                                <input wire:model.defer="imagen" type="file" class="custom-file-input" id="imagen">
                                <label data-browse="Elegir" class="custom-file-label" for="imagen">Seleccionar
                                    imagen...</label>
                                @error('imagen') <span class="text-danger error">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>
                    @if ($tiene_imagen)
                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <div class="form-check">
                                    <input wire:model.defer='quitar_imagen' class="form-check-input" type="checkbox"
                                        id="quitar_imagen">
                                    <label class="form-check-label" for="quitar_imagen">
                                        Eliminar imagen actual (<a href={{ $imagen_url }} target="__blank">Ver
                                            imagen</a>)
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="actualizar"
                    class="btn btn-warning close-modal">
                    <i class="fas fa-check-circle"></i> Actualizar
                </button>
            </div>
        </div>
    </div>
</div>
