<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalEliminarSlider" tabindex="-1" role="dialog"
    aria-labelledby="modalEliminarSliderLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalEliminarSliderLabel">Eliminar slider</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <blockquote class="blockquote">
                    ¿Está seguro de eliminar el registro: {{ $slider->nombre }}?
                </blockquote>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="destruir"
                    class="btn btn-danger close-modal">
                    <i class="fas fa-check-circle"></i> Eliminar
                </button>
            </div>
        </div>
    </div>
</div>
