<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalNuevoSlider" tabindex="-1" role="dialog"
    aria-labelledby="modalNuevoSliderLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalNuevoSliderLabel">Registrar slider</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="nombre">Nombre</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" placeholder="Nombre"
                                wire:model.defer="slider.nombre">
                            @error('slider.nombre') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="imagen">Imagen</label>
                        <div class="col-md-10">
                            <input type="file" class="form-control" placeholder="Imagen"
                                wire:model.defer="imagen">
                            @error('imagen') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="registrar"
                    class="btn btn-primary close-modal">
                    <i class="fas fa-check-circle"></i> Registrar
                </button>
            </div>
        </div>
    </div>
</div>
