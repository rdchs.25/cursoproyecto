<div class="card-body">
    {{-- Mensajes --}}
    @if (session('mensaje'))
        <div class="alert alert-success" role="alert">
            {{ session('mensaje') }}
        </div>
    @endif
    {{-- Formulario de búsqueda --}}
    <div class="form-inline">
        {{-- Boton crear --}}
        <button type="button" data-toggle="modal" data-target="#modalNuevoSlider" class="btn btn-success ml-2 mb-2">
            <i class="fas fa-plus-circle"></i> Nuevo
        </button>
    </div>
    {{-- MODAL DE NUEVO --}}
    @include('livewire.slider.crear')
    {{-- MODAL PARA EDITAR --}}
    @include('livewire.slider.editar')
    {{-- MODAL PARA ELIMINAR --}}
    @include('livewire.slider.eliminar')
    <div class="table-responsive-xl">
        <table class="table table-sm table-bordered table-hover">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listado as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td class="text-center">
                            <img src="{{ $item->imagen_url }}" height="100px" alt="Imagen slider">
                        </td>
                        <td class="text-center">
                            <button wire:click="editar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEditarSlider" class="btn btn-warning ml-2 mb-2">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <button wire:click="eliminar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEliminarSlider" class="btn btn-danger ml-2 mb-2">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="thead-dark">
                <tr class="text-center">
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="table-responsive-xl">
        {{ $listado->links() }}
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        // evento de javascript
        window.livewire.on('cerrarModalNuevoSlider', () => {
            $('#modalNuevoSlider').modal('hide');
        });
        window.livewire.on('cerrarModalEditarSlider', () => {
            $('#modalEditarSlider').modal('hide');
        });
        window.livewire.on('cerrarModalEliminarSlider', () => {
            $('#modalEliminarSlider').modal('hide');
        });

    </script>
@endpush
