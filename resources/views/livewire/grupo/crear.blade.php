<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalNuevoGrupo" tabindex="-1" role="dialog"
    aria-labelledby="modalNuevoGrupoLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="modalNuevoGrupoLabel">Registrar grupo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="tipo_curso_id">Tipo de curso</label>
                        <div class="col-md-8">
                            <select wire:model='tipo_curso_id' class="custom-select" name="tipo_curso_id"
                                id="tipo_curso_id">
                                <option value='0' selected>Seleccionar</option>
                                @foreach ($tipo_cursos as $item)
                                    <option value='{{ $item['id'] }}'>{{ $item['nombre'] }}</option>
                                @endforeach
                            </select>
                            @error('tipo_curso_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="curso_id">Curso</label>
                        <div class="col-md-8">
                            <select wire:model.defer='curso_id' class="custom-select" name="curso_id"
                                id="curso_id">
                                <option value='0' selected>Seleccionar</option>
                                @foreach ($cursos as $item)
                                    <option value='{{ $item['id'] }}'>{{ $item['nombre'] }}</option>
                                @endforeach
                            </select>
                            @error('curso_id') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="nombre">Nombre</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="Nombre"
                                wire:model.defer="nombre">
                            @error('nombre') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="fecha_inicio">Fecha de inicio</label>
                        <div class="col-md-8">
                            <input type="date" class="form-control"
                                wire:model.defer="fecha_inicio">
                            @error('fecha_inicio') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right" for="fecha_fin">Fecha de fin</label>
                        <div class="col-md-8">
                            <input type="date" class="form-control"
                                wire:model.defer="fecha_fin">
                            @error('fecha_fin') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click='reiniciarFormulario()' class="btn btn-secondary close-btn"
                    data-dismiss="modal">
                    <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                </button>
                <button type="button" wire:loading.attr="disabled" wire:click.prevent="registrar"
                    class="btn btn-primary close-modal">
                    <i class="fas fa-check-circle"></i> Registrar
                </button>
            </div>
        </div>
    </div>
</div>
