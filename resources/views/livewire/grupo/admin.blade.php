<div class="card-body">
    {{-- Mensajes --}}
    @if (session('mensaje'))
        <div class="alert alert-success" role="alert">
            {{ session('mensaje') }}
        </div>
    @endif
    {{-- Formulario de búsqueda --}}
    <form class="form-inline" wire:submit.prevent='buscar'>
        <label class="form-label mb-2 mr-sm-2">Tipo de curso</label>
        <select wire:model="bus_tipo_curso_id" class="custom-select mb-2 mr-sm-2">
            <option value="0">Todos</option>
            @foreach ($bus_tipo_cursos as $item)
                <option value="{{ $item['id'] }}">{{ $item['nombre'] }}</option>
            @endforeach
        </select>
        <label class="form-label mb-2 mr-sm-2">Tipo de curso</label>
        <select wire:model.defer="bus_curso_id" class="custom-select mb-2 mr-sm-2">
            <option value="0">Todos</option>
            @foreach ($bus_cursos as $item)
                <option value="{{ $item['id'] }}">{{ $item['nombre'] }}</option>
            @endforeach
        </select>
        <label class="form-label mb-2 mr-sm-2">Nombre</label>
        <input type="text" class="form-control mb-2 mr-sm-2" wire:model.defer="textoBusqueda" />
        <button type="submit" class="btn btn-primary ml-2 mb-2">
            <i class="fas fa-search"></i>
        </button>
        <button type="button" wire:click='crear' data-toggle="modal" data-target="#modalNuevoGrupo"
            class="btn btn-success ml-2 mb-2">
            <i class="fas fa-plus-circle"></i>
        </button>
        <div class="dropdown ml-2 mb-2">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-file"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a target="__blank" href="{{ route('grupo.reportelistadopdf') }}" class="dropdown-item">
                    <i class="fas fa-file-excel"></i> EXCEL
                </a>
                <a target="__blank" href="{{ route('grupo.reportelistadoexcel') }}" class="dropdown-item">
                    <i class="fas fa-file-pdf"></i> PDF
                </a>
            </div>
        </div>
    </form>
    <div class="form-inline">

    </div>
    {{-- MODAL DE NUEVO --}}
    @include('livewire.grupo.crear')
    {{-- MODAL PARA EDITAR --}}
    @include('livewire.grupo.editar')
    {{-- MODAL PARA ELIMINAR --}}
    @include('livewire.grupo.eliminar')
    <div class="table-responsive-xl">
        <table class="table table-sm table-bordered table-hover">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th>Tipo curso</th>
                    <th>Curso</th>
                    <th>Nombre</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listado as $item)
                    <tr>
                        <td class="align-middle">{{ $item->curso->tipoCurso->nombre }}</td>
                        <td class="align-middle">{{ $item->curso->nombre }}</td>
                        <td class="align-middle">{{ $item->nombre }}</td>
                        <td class="align-middle">{{ $item->fecha_inicio->format('d/m/Y') }}</td>
                        <td class="align-middle">
                            {{ is_null($item->fecha_fin) ? '' : $item->fecha_fin->format('d/m/Y') }}
                        </td>
                        <td class="text-center">
                            <button wire:click="editar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEditarGrupo" class="btn btn-warning ml-2 mb-2">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <button wire:click="eliminar({{ $item->id }})" type="button" data-toggle="modal"
                                data-target="#modalEliminarGrupo" class="btn btn-danger ml-2 mb-2">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="thead-dark">
                <tr class="text-center">
                    <th>Tipo curso</th>
                    <th>Curso</th>
                    <th>Nombre</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th colspan="2">Operaciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="table-responsive-xl">
        {{ $listado->links() }}
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        // evento de javascript
        window.livewire.on('cerrarModalNuevoGrupo', () => {
            $('#modalNuevoGrupo').modal('hide');
        });
        window.livewire.on('cerrarModalEditarGrupo', () => {
            $('#modalEditarGrupo').modal('hide');
        });
        window.livewire.on('cerrarModalEliminarGrupo', () => {
            $('#modalEliminarGrupo').modal('hide');
        });

    </script>
@endpush
