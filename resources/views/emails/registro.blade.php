<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro de persona</title>
</head>
<body>
    <img src="{{ $message->embed($ruta_imagen) }}" />
    <h1>Bienvenido</h1>
    <p>
        <strong>Nombre completo</strong>{{ $nombre_completo }}
    </p>
    <p>
        <strong>Dirección</strong>{{ $direccion }}
    </p>
</body>
</html>