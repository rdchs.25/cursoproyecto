<x-app-layout>
    <x-slot name="titulo">
        Grupos
    </x-slot>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header p-3">Gestión de grupos de curso</h5>
                    @livewire('grupo.admin')
                </div>
            </div>
        </div>
    </div>
</x-app-layout>