<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model
{
    use SoftDeletes;

    // relacion con el modelo TipoCurso: tabla "cursos" existe un campo que se llama
    // "tipo_curso_id"
    public function tipoCurso()
    {
        return $this->belongsTo('App\TipoCurso');
    }

    // relacion con la tabla users
    // user_id
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // relacion con la tabla grupos
    // en la tabla "grupos" hay un campo que se llama "curso_id", 
    // que crea la relación con la tabla "cursos"
    public function grupos()
    {
        return $this->hasMany('App\Grupo');
    }

    public function getImagenUrlAttribute()
    {
        if (is_null($this->imagen)) {
            $url = 'imagen_por_defecto.png';
            return asset($url);
        } else {
            $url = 'storage/cursos/' . $this->imagen;
            return asset($url);
        }
    }

    public function getImagenPathAttribute()
    {
        if (is_null($this->imagen)) {
            return null;
        } else {
            return storage_path('app/public/cursos/' . $this->imagen);
        }
    }
}
