<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedSocial extends Model
{
    protected $table = 'redes_sociales';

    public function persona(){
        return $this->belongsTo('App\Persona');
    }
}
