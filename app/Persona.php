<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes;

    public function scopeFiltrarPorTexto($query, $texto)
    {
        return $query->where(function ($consulta) use ($texto) {
            if (!is_null($texto) && $texto !== '') {
                $consulta->where('apellido_paterno', 'LIKE', '%' . $texto . '%')
                    ->orWhere('apellido_materno', 'LIKE', '%' . $texto . '%')
                    ->orWhere('nombres', 'LIKE', '%' . $texto . '%');
            }
        })->orderBy('apellido_paterno', 'ASC')
            ->orderBy('apellido_materno', 'ASC')
            ->orderBy('nombres', 'ASC')
            ->paginate(20);
    }

    public function scopeBuscarPorTexto($query, $texto)
    {
        return $query->where(function ($consulta) use ($texto) {
            if (!is_null($texto) && $texto !== '') {
                $consulta->where('apellido_paterno', 'LIKE', '%' . $texto . '%')
                    ->orWhere('apellido_materno', 'LIKE', '%' . $texto . '%')
                    ->orWhere('nombres', 'LIKE', '%' . $texto . '%');
            }
        })->select('apellido_paterno', 'apellido_materno', 'nombres', 'email')
            ->orderBy('apellido_paterno', 'ASC')
            ->orderBy('apellido_materno', 'ASC')
            ->orderBy('nombres', 'ASC');
    }

    // relación de pertenencia con tabla distritos
    public function distrito()
    {
        return $this->belongsTo('App\Distrito');
    }

    // relación de uno a muchos con tabla matrículas
    public function matriculas()
    {
        return $this->hasMany('App\Matricula');
    }

    // relación de uno a uno con tabla red social
    public function redSocial()
    {
        return $this->hasOne('App\RedSocial');
    }

    public function imagen()
    {
        return $this->morphOne('App\Imagen', 'imagenable');
    }

    public function getNombreCompletoAttribute()
    {
        return $this->apellido_paterno . ' ' . $this->apellido_materno . ', ' . $this->nombres;
    }

    public function getNombreMayusculaAttribute()
    {
        return strtoupper($this->nombres);
    }
}
