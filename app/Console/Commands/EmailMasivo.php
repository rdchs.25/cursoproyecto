<?php

namespace App\Console\Commands;

use App\Mail\EmailRegistro;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailMasivo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:masivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar correos masivos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $usuarios = User::whereNotNull('email_verified_at')->get();
        foreach($usuarios as $usuario){
            Mail::to($usuario->email)
            ->send(new EmailRegistro("Juan Perez", "Calle Los Olivos"));
        }
        return 0;
    }
}
