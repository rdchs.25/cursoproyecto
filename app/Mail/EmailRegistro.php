<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailRegistro extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre_completo;
    public $direccion;
    public $ruta_imagen;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombre, $direccion)
    {
        $this->nombre_completo = $nombre;
        $this->direccion = $direccion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name')->with(['titulo' => 'Titulo']);
        $ruta_adjunto = storage_path('app/mails/adjunto.pdf');
        $this->ruta_imagen = storage_path('app/mails/logo.png');
        return $this->view('emails.registro')
            ->attach($ruta_adjunto, [
                'mime' => 'application/pdf',
                'as' => 'Bienvenido.pdf'
            ]);
    }
}
