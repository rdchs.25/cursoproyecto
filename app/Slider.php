<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    public function getImagenUrlAttribute()
    {
        $url = 'storage/sliders/' . $this->url;
        return asset($url);
    }

    public function getImagenPathAttribute()
    {
        return storage_path('app/public/sliders/' . $this->url);
    }
}
