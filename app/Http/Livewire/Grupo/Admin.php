<?php

namespace App\Http\Livewire\Grupo;

use App\Curso;
use App\Grupo;
use App\TipoCurso;
use Livewire\Component;
use Livewire\WithPagination;

class Admin extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $nombre;
    public $fecha_inicio;
    public $fecha_fin;
    public $curso_id;
    public $tipo_curso_id;
    public $cursos = [];
    public $tipo_cursos = [];
    public $grupo_id = '';
    // atributos para busqueda
    public $textoBusqueda = '';
    public $bus_tipo_curso_id = '0';
    public $bus_tipo_cursos = [];
    public $bus_curso_id = '0';
    public $bus_cursos = [];

    public function updatedBusTipoCursoId()
    {
        if ($this->bus_tipo_curso_id === '0') {
            $this->reset(['bus_curso_id', 'bus_cursos']);
        } else {
            $this->bus_cursos = Curso::where('tipo_curso_id', $this->bus_tipo_curso_id)
                ->select('id', 'nombre')
                ->get()->toArray();
        }
    }

    public function updatedTipoCursoId()
    {
        if ($this->tipo_curso_id === '0') {
            $this->reset('distrito_id');
        } else {
            $this->cursos = Curso::where('tipo_curso_id', $this->tipo_curso_id)
                ->select('id', 'nombre')->get()->toArray();
        }
    }

    public function mount()
    {
        $this->bus_tipo_cursos = TipoCurso::select('id', 'nombre')->get()->toArray();
    }

    public function crear()
    {
        $this->tipo_cursos = TipoCurso::select('id', 'nombre')
            ->get()->toArray();
    }

    public function registrar()
    {
        $this->validate([
            'nombre' => ['required', 'string', 'max:50'],
            'fecha_inicio' => ['required', 'date', 'date_format:Y-m-d'],
            'fecha_fin' => ['nullable', 'date', 'date_format:Y-m-d'],
            'curso_id' => ['required', 'exists:cursos,id'],
            'tipo_curso_id' => ['required', 'exists:tipo_cursos,id'],
        ], [], [
            'curso_id' => 'curso',
            'tipo_curso_id' => 'tipo de curso'
        ]);
        $grupo = new Grupo();
        $grupo->nombre = $this->nombre;
        $grupo->fecha_inicio = $this->fecha_inicio;
        $grupo->fecha_fin = $this->fecha_fin;
        $grupo->curso_id = $this->curso_id;
        $grupo->save();
        session()->flash('mensaje', 'Registrado correctamente');
        $this->reiniciarFormulario();
        $this->resetPage();
        $this->emit('cerrarModalNuevoGrupo');
    }

    public function editar($id)
    {
        $this->grupo_id = $id;
        $grupo = Grupo::find($id);
        $this->nombre = $grupo->nombre;
        $this->fecha_inicio = $grupo->fecha_inicio->format('Y-m-d');
        $this->fecha_fin = (!is_null($grupo->fecha_fin)) ? $grupo->fecha_fin->format('Y-m-d') : '';
        $this->curso_id = $grupo->curso_id;
        $this->tipo_curso_id = $grupo->curso->tipo_curso_id;
        $this->tipo_cursos = TipoCurso::select('id', 'nombre')->get()->toArray();
        $this->cursos = Curso::where('tipo_curso_id', $this->tipo_curso_id)
            ->select('id', 'nombre')->get()->toArray();
    }

    public function actualizar()
    {
        $this->validate([
            'nombre' => ['required', 'string', 'max:50'],
            'fecha_inicio' => ['required', 'date', 'date_format:Y-m-d'],
            'fecha_fin' => ['nullable', 'date', 'date_format:Y-m-d'],
            'curso_id' => ['required', 'exists:cursos,id'],
            'tipo_curso_id' => ['required', 'exists:tipo_cursos,id'],
        ], [], [
            'curso_id' => 'curso',
            'tipo_curso_id' => 'tipo de curso'
        ]);
        $grupo = Grupo::find($this->grupo_id);
        $grupo->nombre = $this->nombre;
        $grupo->fecha_inicio = $this->fecha_inicio;
        $grupo->fecha_fin = $this->fecha_fin;
        $grupo->curso_id = $this->curso_id;
        $grupo->save();
        session()->flash('mensaje', 'Actualizado correctamente');
        $this->reiniciarFormulario();
        $this->resetPage();
        $this->emit('cerrarModalEditarGrupo');
    }

    public function eliminar($id)
    {
        $this->grupo_id = $id;
        $grupo = Grupo::find($this->grupo_id);
        $this->nombre = $grupo->nombre;
    }

    public function destruir()
    {
        Grupo::destroy($this->grupo_id);
        session()->flash('mensaje', 'Eliminado correctamente');
        $this->reiniciarFormulario();
        $this->resetPage();
        $this->emit('cerrarModalEliminarGrupo');
    }

    public function reiniciarFormulario()
    {
        $this->reset(['nombre', 'fecha_inicio', 'fecha_fin', 'curso_id', 'tipo_curso_id', 'grupo_id']);
    }

    public function buscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        $listado = Grupo::join('cursos', 'grupos.curso_id', '=', 'cursos.id')
            ->join('tipo_cursos', 'cursos.tipo_curso_id', '=', 'tipo_cursos.id')->where(function ($query) {
                if ($this->textoBusqueda !== '') {
                    $texto = '%' . $this->textoBusqueda . '%';
                    $query->where('grupos.nombre', 'LIKE', $texto)
                        ->orWhere('tipo_cursos.nombre', 'LIKE', $texto)
                        ->orWhere('cursos.nombre', 'LIKE', $texto);
                }
                if ($this->bus_tipo_curso_id !== '0') {
                    $query->where('tipo_cursos.id', $this->bus_tipo_curso_id);
                }
                if ($this->bus_curso_id !== '0') {
                    $query->where('cursos.id', $this->bus_curso_id);
                }
            })->orderBy('tipo_cursos.nombre', 'ASC')
            ->orderBy('cursos.nombre', 'ASC')->select('grupos.*')->paginate(30);
        return view('livewire.grupo.admin', compact('listado'));
    }
}
