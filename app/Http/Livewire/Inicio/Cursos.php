<?php

namespace App\Http\Livewire\Inicio;

use App\Curso;
use Livewire\Component;
use Livewire\WithPagination;

class Cursos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $tipoCurso = '';

    public function mount($tipoCurso = '')
    {
        $this->tipoCurso = $tipoCurso;
    }
    public function render()
    {
        $cursos = Curso::join('tipo_cursos', 'cursos.tipo_curso_id', '=', 'tipo_cursos.id')
            ->where(function ($query) {
                if ($this->tipoCurso !== '') {
                    $query->where('tipo_cursos.slug', $this->tipoCurso);
                }
            })->select('cursos.*')->orderBy('cursos.nombre', 'ASC')->paginate(30);
        return view('livewire.inicio.cursos', [
            'cursos' => $cursos
        ]);
    }
}
