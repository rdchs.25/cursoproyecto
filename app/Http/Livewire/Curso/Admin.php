<?php

namespace App\Http\Livewire\Curso;

use App\Curso;
use App\TipoCurso;
use Gumlet\ImageResize;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Admin extends Component
{
    use WithPagination;
    use WithFileUploads;
    protected $paginationTheme = 'bootstrap';
    public $nombre = '';
    public $nombre_corto = '';
    public $imagen = null;
    public $user_id = '';
    public $tipo_curso_id = '';
    public $tipo_cursos = [];
    public $textoBusqueda = '';
    public $curso_id = '';
    // cuando se va a editar un curso, se verifica si mostrarle la opción de eliminar 
    // su imagen actual. Si es null actualmente la imagen, se debe quitar esta opción
    public $imagen_url = '';
    public $tiene_imagen = false;
    public $quitar_imagen = false;

    public function mount()
    {
    }

    /**
     * Cuando se de clic sobre el botón de nuevo, se consultará en base
     * de datos la lista de tipos de curso
     *
     * @return void
     */
    public function crear()
    {
        $this->tipo_cursos = TipoCurso::select('id', 'nombre')->get()->toArray();
    }

    /**
     * Validar y registrar en base de datos
     *
     * @return void
     */
    public function registrar()
    {
        $this->validate([
            'nombre' => ['required', 'string', 'max:100'],
            'nombre_corto' => ['required', 'string', 'max:20'],
            'imagen' => ['nullable', 'image'],
            'tipo_curso_id' => ['required', 'exists:tipo_cursos,id']
        ], [],  [
            'tipo_curso_id' => 'tipo de curso'
        ]);
        $curso = new Curso();
        $curso->nombre = $this->nombre;
        $curso->nombre_corto = $this->nombre_corto;
        if (!is_null($this->imagen)) {
            $this->imagen->store('public/cursos');
            $curso->imagen = $this->imagen->hashName();
        }
        $curso->user_id = Auth::user()->id;
        $curso->tipo_curso_id = $this->tipo_curso_id;
        $curso->save();
        // inicio reducir imagen
        if (!is_null($curso->imagen_path)) {
            $pathImagen = $curso->imagen_path;
            $datosImagen = getimagesize($pathImagen);
            $ancho = $datosImagen[0];
            $alto = $datosImagen[1];
            $image = new ImageResize($pathImagen);
            if ($ancho >= 700 && $alto < 400) {
                $image->resizeToWidth(400);
            } elseif ($ancho < 700 && $alto >= 400) {
                $image->resizeToWidth(700);
            } elseif ($ancho < 700 && $alto < 400) {
                $image->resize(700, 400);
            }
            $image->crop(700, 400);
            $image->save($pathImagen);
            // fin reducir imagen
        }

        session()->flash('mensaje', 'Registrado correctamente');
        $this->reiniciarFormulario();
        $this->resetPage();
        $this->emit('cerrarModalNuevoCurso');
    }

    /**
     * Recibe el id de la base de datos y consulta para obtener los datos 
     * del registro
     *
     * @param int $id
     * @return void
     */
    public function editar($id)
    {
        $curso = Curso::find($id);
        $this->curso_id = $id;
        $this->nombre = $curso->nombre;
        $this->nombre_corto = $curso->nombre_corto;
        $this->user_id = $curso->user_id;
        $this->tipo_curso_id = $curso->tipo_curso_id;
        $this->tiene_imagen = (is_null($curso->imagen)) ? false : true;
        $this->imagen_url = $curso->imagen_url;
        $this->tipo_cursos = TipoCurso::select('id', 'nombre')->get()->toArray();
    }

    /**
     * Método para validar y actualizar en base de datos
     *
     * @return void
     */
    public function actualizar()
    {
        $this->validate([
            'nombre' => ['required', 'string', 'max:100'],
            'nombre_corto' => ['required', 'string', 'max:20'],
            'imagen' => ['nullable', 'image'],
            'tipo_curso_id' => ['required', 'exists:tipo_cursos,id']
        ], [],  [
            'tipo_curso_id' => 'tipo de curso'
        ]);
        $curso = Curso::find($this->curso_id);
        $curso->nombre = $this->nombre;
        $curso->nombre_corto = $this->nombre_corto;
        if ($this->quitar_imagen === true || !is_null($this->imagen)) {
            if (!is_null($curso->imagen_path)) {
                File::delete($curso->imagen_path);
                $curso->imagen = null;
            }
        }
        if (!is_null($this->imagen)) {
            $this->imagen->store('public/cursos');
            $curso->imagen = $this->imagen->hashName();
        }

        $curso->user_id = Auth::user()->id;
        $curso->tipo_curso_id = $this->tipo_curso_id;
        $curso->save();
        session()->flash('mensaje', 'Actualizado correctamente');
        $this->reiniciarFormulario();
        $this->resetPage();
        $this->emit('cerrarModalEditarCurso');
    }

    /**
     * Método que consulta en base de datos el registro que se va a 
     * eliminar
     *
     * @param int $id
     * @return void
     */
    public function eliminar($id)
    {
        $this->curso_id = $id;
        $curso = Curso::find($this->curso_id);
        $curso->nombre = $this->nombre;
    }

    /**
     * Método para eliminar de base de datos
     *
     * @return void
     */
    public function destruir()
    {
        Curso::destroy($this->curso_id);
        session()->flash('mensaje', 'Eliminado correctamente');
        $this->reiniciarFormulario();
        $this->resetPage();
        $this->emit('cerrarModalEliminarCurso');
    }

    /**
     * Método para retornar a los valores iniciales los atributos 
     * públicos
     *
     * @return void
     */
    public function reiniciarFormulario()
    {
        $this->reset(['nombre', 'nombre_corto', 'imagen', 'tipo_curso_id', 'tipo_cursos', 'curso_id', 'tiene_imagen', 'imagen_url', 'quitar_imagen']);
    }

    /**
     * Método encargado de refrescar la vista, cada vez que alguno de 
     * los atributos públicos que intervienen en este método, sufre un cambio
     */
    public function render()
    {
        $listado = Curso::where('nombre', 'LIKE', '%' . $this->textoBusqueda . '%')->paginate();
        return view('livewire.curso.admin', compact('listado'));
    }

    /**
     * Método para reiniciar la páginación cuando se hace una nueva búsqueda
     *
     * @return void
     */
    public function buscar()
    {
        $this->resetPage();
    }
}
