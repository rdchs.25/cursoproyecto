<?php

namespace App\Http\Livewire\Persona;

use App\Persona;
use Livewire\Component;

class EditarFila extends Component
{
    public $persona_id;
    public $email;
    public $editable = false;

    public function permitirEditar(){
        $this->editable = !$this->editable;
    }

    public function mount($persona){
        $this->persona_id = $persona->id;
        $this->email = $persona->email;
    }

    public function updatedEmail(){
        $persona = Persona::find($this->persona_id);
        $persona->email = $this->email;
        $persona->save();
    }

    public function render()
    {
        return view('livewire.persona.editar-fila');
    }
}
