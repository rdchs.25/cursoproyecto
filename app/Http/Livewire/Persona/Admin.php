<?php

namespace App\Http\Livewire\Persona;

use App\Departamento;
use App\Distrito;
use App\Exports\PersonaExport;
use App\Jobs\EnvioCorreoRegistro;
use App\Persona;
use App\Provincia;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;
use Rchaname\Consultadni\ConsultaAPI;
use TCPDF;

class Admin extends Component
{
    // para cambiar de página
    use WithPagination;

    // string, integer, array, boolean
    protected $paginationTheme = 'bootstrap';
    public $textoBusqueda;
    // campos de formulario: campos de persona
    public $apellido_paterno = '';
    public $apellido_materno = '';
    public $nombres = '';
    public $celular = '';
    public $email = '';
    public $direccion = '';
    public $distrito_id = '';
    // campos extras
    public $provincia_id = '';
    public $departamento_id = '';
    // los selects (combos)
    public $departamentos = [];
    public $provincias = [];
    public $distritos = [];
    // para actualizar y eliminar
    public $persona_id;
    public $dni;

    public function buscarDni()
    {
        $consultaApi = new ConsultaAPI();
        $datos = $consultaApi->getDniOptimizePeru($this->dni);
        if (count($datos) > 0) {
            $this->apellido_paterno = $datos['apellido_paterno'];
            $this->apellido_materno = $datos['apellido_materno'];
            $this->nombres = $datos['nombres'];
        } else {
            $this->apellido_paterno = '';
            $this->apellido_materno = '';
            $this->nombres = '';
        }
    }

    public function mount()
    {
        $this->departamentos = Departamento::select('id', 'nombre')->get()->toArray();
    }

    // departamento_id => DepartamentoId
    public function updatedDepartamentoId()
    {
        if ($this->departamento_id === '0') {
            $this->reset(['provincia_id', 'distrito_id', 'provincias', 'distritos']);
        } else {
            $this->provincias = Provincia::select('id', 'nombre')->where('departamento_id', $this->departamento_id)->get()->toArray();
            $this->reset(['distrito_id', 'distritos']);
        }
    }

    // Provincia::schema('empresa1')->get();

    // provincia_id => ProvinciaId
    public function updatedProvinciaId()
    {
        if ($this->provincia_id === '0') {
            $this->reset(['distrito_id', 'distritos']);
        } else {
            $this->distritos = Distrito::select('id', 'nombre')->where('provincia_id', $this->provincia_id)->get()->toArray();
        }
    }

    public function nuevo()
    {
        // validar nuestra data
        $this->validate([
            'apellido_paterno' => ['required'],
            'apellido_materno' => ['required'],
            'nombres' => ['required'],
            'email' => ['required'],
            'celular' => ['nullable'],
            'direccion' => ['required'],
            'departamento_id' => ['required'],
            'provincia_id' => ['required'],
            'distrito_id' => ['required'],
        ]);
        $persona = new Persona();
        $persona->apellido_paterno = $this->apellido_paterno;
        $persona->apellido_materno = $this->apellido_materno;
        $persona->nombres = $this->nombres;
        $persona->email = $this->email;
        $persona->celular = $this->celular;
        $persona->direccion = $this->direccion;
        $persona->distrito_id = $this->distrito_id;
        $persona->save();
        $this->emit('cerrarModalNuevoPersona');
        // enviar a la cola de trabajos
        EnvioCorreoRegistro::dispatch(
            $persona->email,
            $persona->nombre_completo,
            $persona->direccion
        );
        // mostrar mensaje de registrado correctamente
        session()->flash('mensaje', 'Registrado correctamente');
        $this->reiniciarFormulario();
    }

    public function buscar()
    {
        // cuando se hace una búsqueda se debe regresar a la página 1,
        // se usa $this->resetPage() que es un método propio de livewire,
        // cuando incluimos "use WithPagination"
        $this->resetPage();
    }

    // el método render se ejecuta cada vez que cambiar algún atributo 
    // público del componente
    public function render()
    {
        $texto = $this->textoBusqueda;
        $personas = Persona::filtrarPorTexto($texto);
        return view('livewire.persona.admin', compact('personas'));
    }

    public function reiniciarFormulario()
    {
        $this->reset([
            'apellido_paterno',
            'apellido_materno',
            'nombres',
            'celular',
            'email',
            'direccion',
            'provincia_id',
            'provincias',
            'departamento_id',
            'distritos',
            'distrito_id'
        ]);
    }

    public function editar($id)
    {
        $persona = Persona::findOrFail($id);
        $this->persona_id = $persona->id;
        $this->apellido_paterno = $persona->apellido_paterno;
        $this->apellido_materno = $persona->apellido_materno;
        $this->nombres = $persona->nombres;
        $this->celular = $persona->celular;
        $this->email = $persona->email;
        $this->direccion = $persona->direccion;
        $this->distrito_id = $persona->distrito_id;
        $this->provincia_id = $persona->distrito->provincia_id;
        $this->departamento_id = $persona->distrito->provincia->departamento_id;
        $this->provincias = Provincia::where('departamento_id', $this->departamento_id)
            ->select('id', 'nombre')->get()->toArray();
        $this->distritos = Distrito::where('provincia_id', $this->provincia_id)
            ->select('id', 'nombre')->get()->toArray();
    }

    public function eliminar($id)
    {
        $persona = Persona::find($id);
        $this->persona_id = $persona->id;
        $this->apellido_paterno = $persona->apellido_paterno;
        $this->apellido_materno = $persona->apellido_materno;
    }

    public function destroy()
    {
        $persona = Persona::find($this->persona_id);
        $persona->delete();
        session()->flash('mensaje', 'Eliminado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalEliminarPersona');
    }

    public function actualizar()
    {
        // validar nuestra data
        $this->validate([
            'apellido_paterno' => ['required'],
            'apellido_materno' => ['required'],
            'nombres' => ['required'],
            'email' => ['required'],
            'celular' => ['nullable'],
            'direccion' => ['required'],
            'departamento_id' => ['required'],
            'provincia_id' => ['required'],
            'distrito_id' => ['required'],
        ]);
        $persona = Persona::find($this->persona_id);
        $persona->apellido_paterno = $this->apellido_paterno;
        $persona->apellido_materno = $this->apellido_materno;
        $persona->nombres = $this->nombres;
        $persona->email = $this->email;
        $persona->celular = $this->celular;
        $persona->direccion = $this->direccion;
        $persona->distrito_id = $this->distrito_id;
        $persona->save();
        $this->emit('cerrarModalEditarPersona');
        // mostrar mensaje de registrado correctamente
        session()->flash('mensaje', 'Actualizado correctamente');
        $this->reiniciarFormulario();
    }

    public function reporteExcel()
    {
        Excel::store(new PersonaExport($this->textoBusqueda), 'personas.xlsx');
        return Storage::disk('local')->download('personas.xlsx');
    }

    public function reportePdf()
    {
        $texto = $this->textoBusqueda;
        include resource_path('reportes/persona/') . "listadoPdf.php";
        return Storage::disk('local')->download('personas.pdf');
    }
}
