<?php

namespace App\Http\Livewire\TipoCurso;

use App\TipoCurso;
use Livewire\Component;
use Livewire\WithPagination;

class Admin extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $tipoCurso;

    protected $rules = [
        'tipoCurso.nombre' => [
            'required',
            'string',
            'max:50'
        ]
    ];

    public function mount()
    {
        $this->tipoCurso = new TipoCurso();
        $this->tipoCurso->nombre = '';
    }

    public function registrar()
    {
        $this->validate([
            'tipoCurso.nombre' => ['required', 'string', 'max:50']
        ], [], [
            'tipoCurso.nombre' => 'nombre'
        ]);
        $this->tipoCurso->save();
        session()->flash('mensaje', 'Registrado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalNuevoTipoCurso');
    }

    public function editar($id)
    {
        $this->tipoCurso = TipoCurso::find($id);
    }

    public function actualizar()
    {
        $this->validate([
            'tipoCurso.nombre' => ['required', 'string', 'max:50']
        ], [], [
            'tipoCurso.nombre' => 'nombre'
        ]);
        $this->tipoCurso->save();
        session()->flash('mensaje', 'Actualizado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalEditarTipoCurso');
    }

    public function eliminar($id)
    {
        $this->tipoCurso = TipoCurso::find($id);
    }

    public function destruir()
    {
        TipoCurso::destroy($this->tipoCurso->id);
        session()->flash('mensaje', 'Eliminado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalEliminarTipoCurso');
    }

    public function reiniciarFormulario()
    {
        $this->tipoCurso = new TipoCurso();
        $this->tipoCurso->nombre = '';
    }

    public function render()
    {
        $listado = TipoCurso::paginate(30);
        return view('livewire.tipo-curso.admin', compact('listado'));
    }
}
