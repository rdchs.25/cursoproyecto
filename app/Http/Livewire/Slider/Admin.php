<?php

namespace App\Http\Livewire\Slider;

use App\Slider;
use Gumlet\ImageResize;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Admin extends Component
{
    use WithPagination;
    use WithFileUploads;

    protected $paginationTheme = 'bootstrap';

    public $slider;
    public $imagen;

    protected $rules = [
        'slider.nombre' => [
            'required',
            'string',
            'max:50'
        ],
        'slider.url' => [
            'required',
            'string',
        ]
    ];

    public function mount()
    {
        $this->slider = new Slider();
        $this->slider->nombre = '';
        $this->slider->url = '';
    }

    public function registrar()
    {
        $this->validate([
            'slider.nombre' => ['required', 'string', 'max:50'],
            'imagen' => ['required', 'nullable',]
        ], [], [
            'slider.nombre' => 'nombre',
            'imagen' => 'imagen'
        ]);
        $this->imagen->store('public/sliders');
        $this->slider->url = $this->imagen->hashName();
        $this->slider->save();
        // inicio reducir imagen
        $pathImagen = $this->slider->imagen_path;
        $datosImagen = getimagesize($pathImagen);
        $ancho = $datosImagen[0];
        $alto = $datosImagen[1];
        $image = new ImageResize($pathImagen);
        if($ancho >= 900 && $alto < 350){
            $image->resizeToWidth(350);
        }elseif($ancho < 900 && $alto >= 350){
            $image->resizeToWidth(900);
        }elseif($ancho < 900 && $alto < 350){
            $image->resize(900, 350);
        }
        $image->crop(900, 350);
        $image->save($pathImagen);
        // fin reducir imagen
        session()->flash('mensaje', 'Registrado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalNuevoSlider');
    }

    public function editar($id)
    {
        $this->slider = Slider::find($id);
    }

    public function actualizar()
    {
        $this->validate([
            'slider.nombre' => ['required', 'string', 'max:50'],
            'slider.url' => ['required', 'nullable',]
        ], [], [
            'slider.nombre' => 'nombre',
            'slider.url' => 'imagen'
        ]);
        $this->slider->save();
        session()->flash('mensaje', 'Actualizado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalEditarSlider');
    }

    public function eliminar($id)
    {
        $this->slider = Slider::find($id);
    }

    public function destruir()
    {
        Slider::destroy($this->slider->id);
        session()->flash('mensaje', 'Eliminado correctamente');
        $this->reiniciarFormulario();
        $this->emit('cerrarModalEliminarSlider');
    }

    public function reiniciarFormulario()
    {
        $this->slider = new Slider();
        $this->slider->nombre = '';
        $this->slider->url = '';
    }

    public function render()
    {
        $listado = Slider::paginate(30);
        return view('livewire.slider.admin', compact('listado'));
    }
}
