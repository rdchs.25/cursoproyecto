<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Persona;
use Illuminate\Http\Request;

/**
* @OA\Info(title="API para información de persosas", version="1.0")
*
* @OA\Server(url="http://localhost/intermedio/public")
*/
class PersonaController extends Controller
{
    /**
    * @OA\Get(
    *     path="/api/personas",
    *     summary="Mostrar personas",
    *     @OA\RequestBody(
    *    required=true,
    *    description="Pass user credentials",
    *    @OA\JsonContent(
    *       required={"texto"},
    *       @OA\Property(property="texto", type="string", format="string", example="Jorge"),
    *    ),
    * ),
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todas las personas."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function index(Request $request)
    {
        return Persona::filtrarPorTexto($request->input("texto"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
