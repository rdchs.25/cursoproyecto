<?php

namespace App\Http\Controllers;

class TipoCursoController extends Controller
{
    protected $pathReportes;

    public function __construct()
    {
        $this->pathReportes = resource_path('reportes/tipocurso/');
    }

    public function index()
    {
        return view('admin.tipocurso.index');
    }

    public function reporteListadoPdf()
    {
        include $this->pathReportes . "listadoPdf.php";
    }

    public function reporteListadoExcel()
    {
        include $this->pathReportes . "listadoExcel.php";
    }
}
