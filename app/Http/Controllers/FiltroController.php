<?php

namespace App\Http\Controllers;

use App\Curso;
use Illuminate\Http\Request;

class FiltroController extends Controller
{
    public function tipoCurso($tipoCurso)
    {
        return view('categorias', [
            'tipoCurso' => $tipoCurso
        ]);
    }
}
