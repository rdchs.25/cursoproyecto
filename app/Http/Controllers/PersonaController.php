<?php

namespace App\Http\Controllers;

class PersonaController extends Controller
{
    protected $pathReportes;

    public function __construct()
    {
        $this->pathReportes = resource_path('reportes/persona/');
    }
    
    public function index()
    {
        return view('admin.persona.index');
    }

    public function reporteListadoPdf()
    {
        include $this->pathReportes. "listadoPdf.php";
    }

    public function reporteListadoExcel()
    {
        include $this->pathReportes. "listadoExcel.php";
    }
}
