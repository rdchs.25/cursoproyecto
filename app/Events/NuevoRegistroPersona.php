<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NuevoRegistroPersona implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $usuario;
    private $nombreRegistrado;
    private $numeroActual;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $usuario, $nombrePersona, $numeroActual)
    {
        // tabla me_gusta
        // $megusta = new Megusta();
        // $megusta->numero = 101;
        // $megusta->save();
        $this->numeroActual = $numeroActual;
        $this->usuario = $usuario;
        $this->nombreRegistrado = $nombrePersona;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $nombre_canal = 'nuevo-registro.'.$this->usuario->id;
        return new PrivateChannel($nombre_canal);
    }

    public function broadcastAs(){
        return "CanalNuevosRegistros";
    }

    public function broadcastWith(){
        return [
            "mensaje" => "Se ha registrado: ".$this->nombreRegistrado,
            "numero" => $this->numeroActual
        ];
    }
}
