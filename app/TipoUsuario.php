<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    public function usuarios()
    {
        return $this->belongsToMany('App\User', 'permisos');
    }
}
