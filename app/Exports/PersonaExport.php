<?php

namespace App\Exports;

use App\Persona;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PersonaExport implements FromQuery, WithHeadings, ShouldAutoSize, WithStyles
{
    private $textoBusqueda;
    private $cantidad = 30;

    public function __construct($busqueda)
    {
        $this->textoBusqueda = $busqueda;
    }

    public function headings(): array
    {
        return [
            'Apellido paterno',
            'Apellido materno',
            'Nombres',
            'Email',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:D1')->getFont()->setBold(true);
        $sheet->getStyle('A1:D1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFF0000');;
        $sheet->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $sheet->getStyle('A2:D' . $this->cantidad)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    }

    public function query()
    {
        $resultados = Persona::buscarPorTexto($this->textoBusqueda);
        $this->cantidad = $resultados->count() + 1;
        return $resultados;
    }
}
