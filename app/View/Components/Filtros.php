<?php

namespace App\View\Components;

use App\TipoCurso;
use Illuminate\View\Component;

class Filtros extends Component
{
    public $tipos;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tipos = TipoCurso::all();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.filtros');
    }
}
