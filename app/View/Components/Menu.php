<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\Component;

class Menu extends Component
{
    public $listaMenu;
    public $opcionActiva;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->opcionActiva = Route::currentRouteName();
        $this->listaMenu = [
            ['nombre' => 'Personas', 'ruta' => 'persona.index'],
            ['nombre' => 'Curso', 'ruta' => 'curso.index'],
            ['nombre' => 'Tipos de curso', 'ruta' => 'tipocurso.index'],
            ['nombre' => 'Grupos de curso', 'ruta' => 'grupo.index'],
            ['nombre' => 'Sliders', 'ruta' => 'slider.index'],
        ];
        if (Auth::user()->tipo_usuario_id === 2) {
            $this->listaMenu = [
                ['nombre' => 'Mis cursos', 'ruta' => 'matricula.index'],
            ];
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.menu');
    }
}
