<?php

namespace App\Jobs;

use App\Mail\EmailRegistro;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EnvioCorreoRegistro implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $nombre_completo;
    protected $direccion;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $nombre, $direccion)
    {
        $this->email = $email;
        $this->nombre_completo = $nombre;
        $this->direccion = $direccion;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Enviado');
        // Mail::to($this->email)
        //     ->send(new EmailRegistro($this->nombre_completo, $this->direccion));
    }
}
