<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('nuevo-registro.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

// Juan => nuevo-registro.1
// Pedro => nuevo-registro.209
// Luis => nuevo-registro.450
// Jose => nuevo-registro.120
