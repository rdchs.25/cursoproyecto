<?php

use App\Events\MensajePrivado;
use App\Events\MensajePublico;
use App\Events\NuevoRegistroPersona;
use App\Persona;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@index')->name('inicio');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    // persona
    Route::get('persona', 'PersonaController@index')->name('persona.index');
    Route::get('persona/reporte/listado/pdf', 'PersonaController@reporteListadoPdf')->name('persona.reportelistadopdf');
    Route::get('persona/reporte/listado/excel', 'PersonaController@reporteListadoExcel')->name('persona.reportelistadoexcel');
    // tipocurso
    Route::get('tipocurso', 'TipoCursoController@index')->name('tipocurso.index');
    Route::get('tipocurso/reporte/listado/pdf', 'TipoCursoController@reporteListadoPdf')->name('tipocurso.reportelistadopdf');
    Route::get('tipocurso/reporte/listado/excel', 'TipoCursoController@reporteListadoExcel')->name('tipocurso.reportelistadoexcel');
    // curso
    Route::get('curso', 'CursoController@index')->name('curso.index');
    Route::get('curso/reporte/listado/pdf', 'CursoController@reporteListadoPdf')->name('curso.reportelistadopdf');
    Route::get('curso/reporte/listado/excel', 'CursoController@reporteListadoExcel')->name('curso.reportelistadoexcel');
    // grupo
    Route::get('grupo', 'GrupoController@index')->name('grupo.index');
    Route::get('grupo/reporte/listado/pdf', 'GrupoController@reporteListadoPdf')->name('grupo.reportelistadopdf');
    Route::get('grupo/reporte/listado/excel', 'GrupoController@reporteListadoExcel')->name('grupo.reportelistadoexcel');
});

// sliders
Route::get('slider', 'SliderController@index')->name('slider.index');
// filtros
Route::get('tipocurso/{tipoCurso}', 'FiltroController@tipoCurso')->name('filtro.tipocurso.index');

Route::get('emitirevento', function () {
    $user = User::find(1);
    // crear tabla para eventos
    event(new NuevoRegistroPersona($user, "Jose Perez", 209));
    return "ok";
});

Route::get('generartoken', function(){
    $user = User::find(1);
    $token = $user->createToken('token-para-aplicacion-movil');
    return $token->plainTextToken;
});
