require('dotenv').config()
const mix = require('laravel-mix');

const resourceRoot = process.env.LARAVEL_MIX_RESOURCE_ROOT || null;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .styles('resources/css/shop-homepage.css', 'public/css/shop-homepage.css')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/icons.scss', 'public/css').sourceMaps();

if (resourceRoot !== null) {
    mix.setResourceRoot(resourceRoot);
}
