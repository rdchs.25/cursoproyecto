<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Curso;
use Faker\Generator as Faker;

$factory->define(Curso::class, function (Faker $faker) {
    return [
        'nombre' => $faker->text(50),
        'nombre_corto' => $faker->text(20),
        'user_id' => $faker->randomElement([1]),
        'tipo_curso_id' => $faker->randomElement([1, 2, 3, 4]),
    ];
});
