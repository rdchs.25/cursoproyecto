<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Distrito;
use App\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {
    return [
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' => $faker->lastName,
        'nombres' => $faker->firstName,
        'email' => $faker->safeEmail,
        'direccion' => $faker->address,
        'celular' => $faker->phoneNumber,
        'distrito_id' => Distrito::inRandomOrder()->first()->id
    ];
});
