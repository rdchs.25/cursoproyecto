<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoCurso;
use Faker\Generator as Faker;

$factory->define(TipoCurso::class, function (Faker $faker) {
    return [
        'nombre' => $faker->text(50)
    ];
});
