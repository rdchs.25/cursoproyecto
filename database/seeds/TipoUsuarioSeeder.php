<?php

use App\TipoCurso;
use App\TipoUsuario;
use Illuminate\Database\Seeder;

class TipoUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoUsuario = new TipoUsuario();
        $tipoUsuario->nombre = 'Administrador';
        $tipoUsuario->save();

        $tipoUsuario2 = new TipoUsuario();
        $tipoUsuario2->nombre = 'Alumno';
        $tipoUsuario2->save();
    }
}
