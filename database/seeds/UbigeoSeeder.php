<?php

use App\Departamento;
use App\Distrito;
use App\Provincia;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UbigeoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamentos = DB::connection('mysql_ubigeo')->table('ubigeo_peru_departments')->get();
        foreach($departamentos as $dep){
            $departamento =  new Departamento();
            $departamento->nombre = $dep->name;
            $departamento->save();
            $provincias = DB::connection('mysql_ubigeo')->table('ubigeo_peru_provinces')->where('department_id', $dep->id)->get();
            foreach ($provincias as  $prov) {
                $provincia = new Provincia();
                $provincia->nombre = $prov->name;
                $provincia->departamento_id = $departamento->id;
                $provincia->save();
                $distritos =  DB::connection('mysql_ubigeo')->table('ubigeo_peru_districts')->where('province_id', $prov->id)->get();
                foreach ($distritos as $dist) {
                    $distrito = new Distrito();
                    $distrito->nombre = $dist->name;
                    $distrito->provincia_id = $provincia->id;
                    $distrito->save();
                }
            }
        }
    }
}
