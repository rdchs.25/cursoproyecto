<?php

use Illuminate\Database\Seeder;
use App\User;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create(
            [
                'name' => 'Administrador del Sistema',
                'email' => 'admin@gmail.com',
                'tipo_usuario_id' => 1
            ]
        );
    }
}
