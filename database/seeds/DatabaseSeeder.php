<?php

use App\Curso;
use App\Grupo;
use App\TipoCurso;
use App\TipoUsuario;
use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TipoUsuarioSeeder::class);
        $this->call(UsuarioSeeder::class);
        $this->call(UbigeoSeeder::class);
        $this->call(PersonaSeeder::class);
        $this->call(TipoCursoSeeder::class);
        $this->call(CursoSeeder::class);
        // factory(User::class, 4)->create();
        // factory(TipoCurso::class, 4)->create();
        // factory(Curso::class, 50)->create();
        // factory(Grupo::class, 10)->create();
        // factory(TipoUsuario::class, 5)->create();
    }
}
