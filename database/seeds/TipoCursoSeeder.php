<?php

use App\TipoCurso;
use Illuminate\Database\Seeder;

class TipoCursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoCursos = [
            'Diplomado',
            'Certificación',
            'Taller',
            'Curso Taller',
            'Charla',
            'Curso',
        ];
        foreach($tipoCursos as $tipoCurso){
            $registro = new TipoCurso();
            $registro->nombre = $tipoCurso;
            $registro->save();
        }
    }
}
