<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->id(); // AUTOINCREMENT, "id", UNSIGNED, BIG INTEGER
            $table->string('nombre', 100);
            $table->string('nombre_corto', 20);
            $table->string('imagen')->nullable();
            // forma larga o mas específica
            $table->foreignId('user_id')->constrained();
            $table->unsignedBigInteger('tipo_curso_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('tipo_curso_id')->references('id')->on('tipo_cursos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
