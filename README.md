# Proyecto PHP Intermedio

### Paso 01: Instalar dependencias de PHP

```
composer install
```

### Paso 02: Crear archivo .env (copiar contenido de archivo .env.example)

### Paso 03: Completar parámetros de base de datos

```
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

```

### Paso 04: Generar APP_KEY

```
php artisan key:generate
```

### Paso 05: Migrar base de datos

```
php artisan migrate
```

### Paso 06: Si la aplicación no funcionará en la raíz del un dominio (ejemplo: sistemas.cet.org.pe/proyecto/public), completar datos necesarios para Laravel Livewire y usar icons Font Awesome. Dejar sin completar en caso la aplicación funcion en la raíz de un dominio (ejemplo: sistemas.cet.org.pe)

```
LIVEWIRE_ASSETS_URL="/proyecto/public"
LARAVEL_MIX_RESOURCE_ROOT="/proyecto/public/"
```

### Paso 07: Instalar dependencias y generar archivos estáticos (.css, .js, etc) del proyecto

```
npm install
mpm run dev
```

### Paso 08: Crear enlace simbólico para archivos subidos

```
php artisan storage:link
```

### Paso 09: Ejecutar semillas para generación de data de prueba. Descargar la base de datos ubigeo en <a href='https://drive.google.com/file/d/1oSONtDATYIkT4h8M9w_yGgPpxnNdtssY/view?usp=sharing'>este enlace</a> y restaurar. Esta base de datos es necesaria para las semillas de ubigeo. En caso de darle un nombre distinto a esta base de datos de ubigeo, configurar en el archivo config/database.php

```
php artisan db:seed
```